#ifndef VARIABLE_H
#define VARIABLE_H

#include <string>
#include <vector>
#include "symbols.h"

class Variable {

    std::vector<Variable> var_table;

public:
    std::string name;
    double value;
    bool var;	// variable (true) or constant (false)
    Variable (std::string n, double v, bool va = true) :name(n), value(v), var(va) { }



};


#endif // VARIABLE_H
