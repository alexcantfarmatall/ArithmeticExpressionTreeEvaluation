#include "arithmeticexpressionparsing.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    ArithmeticExpressionParsing w;
    w.show();

    return a.exec();
}
