#include "variable_table.h"
#include "variable.h"
#include <iostream>
#include <string>
#include "symbols.h"

double Variable_table::get_value(std::string s)
    // return the value of the Variable names s
{
    for (int i = 0; i<var_table.size(); ++i)
        if (var_table[i].name == s) return var_table[i].value;
    std::cerr << "error adding var " << s << std::endl;
}

//------------------------------------------------------------------------------

void Variable_table::set_value(std::string s, double d)
    // set the Variable named s to d
{
    for (int i = 0; i<var_table.size(); ++i)
        if (var_table[i].name == s) {
            if (var_table[i].var==false) std::cerr << s << " is a constant" << std::endl;
            var_table[i].value = d;
            return;
        }
    std::cerr<< "set: undefined variable " << s << std::endl;
}

//------------------------------------------------------------------------------

bool Variable_table::is_declared(std::string var)
    // is var already in var_table?
{
    for (int i = 0; i<var_table.size(); ++i)
        if (var_table[i].name == var) return true;
    return false;
}

//------------------------------------------------------------------------------

double Variable_table::define_name(std::string s, double val, bool var=true)
    // add (s,val,var) to var_table
{
    if (is_declared(s)) std::cerr << s << ", declared twice" << std::endl;
    var_table.push_back(Variable(s,val,var));
    return val;
}
