#ifndef ARITHMETICEXPRESSIONPARSING_H
#define ARITHMETICEXPRESSIONPARSING_H

#include <QMainWindow>
#include "expressiontree.h"
namespace Ui {
class ArithmeticExpressionParsing;
}

class ArithmeticExpressionParsing : public QMainWindow
{
    Q_OBJECT

public:
    explicit ArithmeticExpressionParsing(QWidget *parent = 0);
    ~ArithmeticExpressionParsing();

private slots:
    void on_pushButton_clicked();

private:
    Ui::ArithmeticExpressionParsing *ui;
    ExpressionTree *expressiontree;

};

#endif // ARITHMETICEXPRESSIONPARSING_H
