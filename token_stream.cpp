#include "token_stream.h"
#include "token.h"
#include <sstream>
#include <iostream>
#include "symbols.h"


//From stroustrup book mostly
//see if theres still stuff in the stream coming
bool Token_stream::isEmpty()
{
    if(ss.good())
    {
        std::cout << "in isEmpty() = true" << std::endl;

        return false;
    }
    std::cout << "in isEmpty()" << std::endl;

    return true;
}


// The constructor just sets full to indicate that the buffer is empty:
Token_stream::Token_stream(std::string expr)
{

    ss.str(expr);
    std::cout << " in ts ctor " << ss.str() << std::endl;
    full = false;
}

//------------------------------------------------------------------------------

// The putback() member function puts its argument back into the Token_stream's buffer:
void Token_stream::putback(Token t)
{
    if (full) std::cerr << "putback() into a full buffer" << std::endl;
    buffer = t;       // copy t to buffer
    full = true;      // buffer is now full
}

//------------------------------------------------------------------------------

Token Token_stream::get() // read characters from cin and compose a Token
{
    std::cout << "get(): ss.str() =  " << ss.str() << std::endl;

    if (full) {    // check if we already have a Token ready
        std::cout << "full" << std::endl;
        full=false;
        return buffer;
    }

    char ch;

    ss >> ch;          // note that >> skips whitespace (space, newline, tab, etc.)

    std::cout << "get(): char ch =  " << ch << std::endl;
    switch (ch) {
    case '(':
    case ')':
    case '+':
    case '-':
    case '*':
    case '/':
    case '%':
    case '=':
    case '^':
        return Token(ch); // let each character represent itself
    case '.':             // a floating-point literal can start with a dot
    case '0': case '1': case '2': case '3': case '4':
    case '5': case '6': case '7': case '8': case '9':    // numeric literal
    {
        ss.putback(ch);// put digit back into the input stream
        double val;
        ss >> val;     // read a floating-point number
        return Token(number,val);
    }
    default:
        if (isalpha(ch)) {	// start with a letter
            std::string s;
            s += ch;
            while (ss.get(ch) && (isalpha(ch) || isdigit(ch) || ch=='_')) s+=ch;	// letters digits and underscores
            ss.putback(ch);
            if (s == declkey) return Token(let); // keyword "let"
            if (s == constkey) return Token(con); // keyword "const"
            return Token(name,s);
        }
        std::cerr << "Bad token" << std::endl;
    }
    std::cout << "magic" << std::endl;
}

//------------------------------------------------------------------------------

void Token_stream::ignore(char c)
    // c represents the kind of a Token
{
    // first look in buffer:
    if (full && c==buffer.kind) {
        full = false;
        return;
    }
    full = false;

    // now search input:
    char ch = 0;
    while (ss>>ch)
        if (ch==c) return;
}
