#include "arithmeticexpressionparsing.h"
#include "ui_arithmeticexpressionparsing.h"
#include <QString>
#include <string>
#include "expressiontree.h"
#include <vector>
#include "token.h"
#include "token_stream.h"
#include <iostream>
#include "symbols.h"
ArithmeticExpressionParsing::ArithmeticExpressionParsing(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::ArithmeticExpressionParsing)
{
    ui->setupUi(this);
}

ArithmeticExpressionParsing::~ArithmeticExpressionParsing()
{
    delete ui;
}

void ArithmeticExpressionParsing::on_pushButton_clicked()
{
    std::string expression = (ui->lineEditExpression->text()).toStdString();

//    std::cout << expression << std::endl;
    std::vector<Token> tokens;
    Token_stream ts(expression);

    while(!ts.isEmpty())
    {
//        std::cout << "looping " << std::endl;
        tokens.push_back(ts.get());
    }

    this->expressiontree = new ExpressionTree(expression);

    double result = this->expressiontree->evaluate(expressiontree->build_tree(tokens));
    std::cout <<"result: "<< result << std::endl;
    ui->lineEdit_2->setText(QString::fromStdString(std::to_string(result)));


    expressiontree->tree_graph(0,0,expressiontree->root);

    ui->graphicsView_tree->setScene(expressiontree->tree_scene);
    ui->graphicsView_tree->show();


}
