#ifndef TOKEN_STREAM_H
#define TOKEN_STREAM_H

#include "token.h"
#include "token_stream.h"
#include "symbols.h"
#include <sstream>

class Token_stream {
public:
    Token_stream(){}
    Token_stream(std::string);   // make a Token_stream that reads from cin
    Token get();      // get a Token (get() is defined elsewhere)
    void putback(Token t);    // put a Token back
    void ignore(char c);// discard tokens up to an including a c
    bool isEmpty();
private:
    bool full;        // is there a Token in the buffer?
    Token buffer;     // here is where we keep a Token put back using putback()
    std::stringstream ss;



};

#endif // TOKEN_STREAM_H
