#include "node.h"
#include "symbols.h"
#include <exception>
Node::Node(Token& t)
{
    this->weight = getWeight(t.kind);
    this->tkind = t.kind;
    this->tval = t.value;
    this->left = nullptr;
    this->right = nullptr;
}

Node* Node::operator=(Node& n)
{
    this->tkind = n.tkind;
    this->tval = n.tval;

    this->left = n.left;
    this->right = n.right;

    return this;
}
/*
Node* Node::operator=(Node* n)
{
    this->token = n->token;
    this->left = n->left;
    this->right = n->right;

    return this;
}
*/
bool Node::operator<(const Node& n)const
{
    return weight > n.weight;
}

double Node::getWeight(char& c){
    switch(c)
    {
    case '+':
    case '-':
    {

        return 1;
    }
    case '*':
    case '/':
    {

        return 2;
    }
    case '^':
    {
        return 3;
    }
    default:
        return 0;
    }
}

int Node::height()
{

    int lh = 0;
    int rh = 0;
    if(this->left){
        lh = this->left->height();

    }
    if(this->right){
        rh = this->right->height();

    }

    return lh > rh ? lh+1 : rh+1;
}

//wrong but works...
int Node::to_left_edge()
{
    int tle = 0;
    if(this->left)
    {
        tle = this->left->height();
    }
    return tle + 1;
}
int Node::to_right_edge()
{
    int tre = 0;
    if(this->right)
    {
        tre = this->left->height();
    }
    return tre + 1;
}

void Node::set_root(){
    if(!this->parent)
        this->root = true;
    else
        throw std::exception();
}

int Node::from_root_node()
{
    int result = 0;
    if(this->parent)
    {
        result = this->parent->from_root_node();
    }
    return result + 1;
}

