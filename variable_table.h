#ifndef VARIABLE_TABLE_H
#define VARIABLE_TABLE_H

#include <vector>
#include <string>
#include "variable.h"
#include "symbols.h"

class Variable_table
{
    std::vector<Variable> var_table;

public:
    Variable_table();

     double get_value(std::string);
     void set_value(std::string, double);
     bool is_declared(std::string);
     double define_name(std::string, double, bool);
};

#endif // VARIABLE_TABLE_H
