#include "expressiontree.h"
#include "node.h"
#include <stack>
#include <string>
#include <iostream>
#include <vector>
#include "symbols.h"
#include "token.h"
#include <exception>
#include <QGraphicsItem>
#include <QGraphicsSimpleTextItem>
#include <math.h>
#include <QString>
#include <QFont>
#include <QGraphicsTextItem>
#include <QGraphicsScene>
#include <QBrush>
#include <sstream>
#include <iomanip>
#include <math.h>
#include "token_stream.h"
ExpressionTree::ExpressionTree(std::string expression){


    tree_scene = new QGraphicsScene();
    tree_tittle =
            new QGraphicsTextItem(QString::fromStdString(expression));
    this->tree_tittle->setPos(0,0);
    tree_scene->addItem(tree_tittle);
}

ExpressionTree::~ExpressionTree()
{
    delete tree_scene;
}


Node* ExpressionTree::build_tree(std::vector<Token>& tokens)
{
    Node* result;
    std::stack<Node*> stack;
    Node* n;

    //put token_stream here, do whwile(!isEmpty(), get)
    for(Token t : tokens)
    {
        if(t.kind == number)
        {
            n = new Node(t);
            stack.push(n); //numbers go to stack to wait for their operators
        }
        /*
        else if(t.kind == ')')
        {
        }
        */
        else//operator
        {
            n = new Node(t);



            n->left = stack.top();//stack top is given to operator

            stack.pop();

            if(!stack.empty())
            {
                while(!stack.empty())
                {
                    if(stack.top()->weight >= n->weight)//top of stack didnt belong to this operator, because stack top has
                                                        //greater or equal precedence, they get to eval first and equals are evaluated in the
                                                        //order of appeareance.
                    {
                        stack.top()->right = n->left;

                        n->left = stack.top(); //stack top is heavier so it belongs to n
                        stack.top()->parent = n;

                        stack.pop(); //stack top is with n, so off the stack
                    }
                    else
                    {
                        break;
                    }
                }
            }
            stack.push(n); // n to stack
        }
    }
    //last number is still in the stack
    while(!stack.empty())
    {
        //1*2+4/5 - 2/6

        //last one must be a number...
        result = stack.top();
        stack.pop();
        if(!stack.empty())
        {
            stack.top()->right = result;
            //result->parent = stack.top();
        }
    }


    this->root = result;

    //result->set_root();

    return result;


}

bool ExpressionTree::isOp(char c)
{
    switch(c)
    {
    case '+':
    case '-':
    case '*':
    case '/':
    case '^':
    {
        return true;
    }
    default:
        return false;
    }
}



double ExpressionTree::calculate(char op, double a, double b)
{
    switch(op)
    {
    case '+':
        return a + b;
    case '-':
    {
        return a - b;
    }
    case '*':
        return a * b;
    case '/':
    {
        if(b > 0)
            return a / b;
        else
            throw std::exception();
    }
    case '^':
    {
        return std::pow(a,b);
    }
    default:
        throw std::exception();
    }
}
//1*2+4/5 - 2/6 -5
double ExpressionTree::evaluate(Node* n)
{
    double result = 0;
    if(n->left && n->right)
    {
        result = calculate(n->tkind,evaluate(n->left),evaluate(n->right));
        return result;
    }
    else if(n->left)
    {
        result = evaluate(n->left);
        return result;
    }
    else if(n->right)
    {
        result = evaluate(n->right);
        return result;
    }
    return n->tval;
}

void ExpressionTree::tree_graph(double x,double y, Node* n)
{

    QGraphicsEllipseItem* node_circle=
            new QGraphicsEllipseItem(
                x - node_circle_radius, y - node_circle_radius, node_circle_radius*2, node_circle_radius*2);
    node_circle->setZValue(1);
    node_circle->setParentItem(this->tree_tittle);
    node_circle->setBrush(QBrush(Qt::lightGray));
    tree_scene->addItem(node_circle);

    auto format_double = [](double d){
        std::stringstream ss;
        ss << std::fixed << std::setprecision(2) << d;
        return ss.str();

    };

    std::string symbol = isOp(n->tkind) ? std::string(1,n->tkind) : format_double(n->tval);

    QGraphicsTextItem* node_symbol =
            new QGraphicsTextItem(QString::fromStdString(symbol));
    node_symbol->setPos(x - node_circle_radius, y - node_circle_radius);
    node_symbol->setZValue(2);
    node_symbol->setFont(QFont("Times", 10, QFont::Bold));
    node_symbol->setParent(this->tree_tittle);
    tree_scene->addItem(node_symbol);
    if(n->left)
    {
        // to function(x,y,n)...
        double anglex = std::cos(M_PI/4) * - n->left->to_left_edge() * node_line_lenght +x ;
        double angley = std::sin(M_PI/4) * n->left->to_left_edge() * node_line_lenght + y;

        QGraphicsLineItem* left_line = new QGraphicsLineItem(x,y,anglex,angley);
        left_line->setPen(QPen(Qt::DotLine));
        left_line->setZValue(0);
        left_line->setParentItem(this->tree_tittle);
        //...function
        tree_scene->addItem(left_line);
        tree_graph(anglex,angley, n->left);

    }
    if(n->right)
    {
        // to function(x,y,n)
        double anglex = std::cos(M_PI/4) * n->right->to_right_edge() * node_line_lenght + x;
        double angley = std::sin(M_PI/4) * n->right->to_right_edge() * node_line_lenght + y;
        QGraphicsLineItem* right_line = new QGraphicsLineItem(x,y,anglex,angley);
        right_line->setParentItem(this->tree_tittle);
        right_line->setPen(QPen(Qt::DotLine));
        right_line->setZValue(0);
        tree_scene->addItem(right_line);
        tree_graph(anglex,angley, n->right);
    }

    return;

}







