#ifndef NODE_H
#define NODE_H

#include "node.h"
#include "token.h"
#include "symbols.h"

class Node
{
     double getWeight(char&);
     bool root = false;


public:
    Node();
    Node* left;
    Node* right;
    Node* parent;
    double weight; //1 for + and -, 2 for * and /, 3 for ^, 4 for (. (make into enum)
    void set_root();//do some checking for validity...
    int height();
    int to_left_edge();
    int to_right_edge();
    int from_root_node();

    //debugging
    char tkind;
    double tval;

    Node* operator=(Node&);
    Node* operator=(Node*);
    bool operator<(const Node&)const;
    Node(Token&);
    ~Node();

};

#endif // NODE_H
