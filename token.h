#ifndef TOKEN_H
#define TOKEN_H

//srtoustrup
#include <string>
#include "symbols.h"
class Token {
public:

    char kind;        // what kind of token
    double value;     // for numbers: a value
    std::string name;      // for names: name itself
    Token():kind('0'), value(0){}
    Token(char ch)             : kind(ch), value(0)   {}
    Token(char ch, double val) : kind(ch), value(val) {}
    Token(char ch, std::string n)   : kind(ch), name(n)    {}



};

#endif // TOKEN_H
