#ifndef SYMBOLS_H
#define SYMBOLS_H
#include <string>


const char quit   = 'q';    // t.kind==quit means that t is a quit Token
const char print  = ';';    // t.kind==print means that t is a print Token
const char name   = 'a';    // name token
const char let    = 'L';    // declaration token
const char con    = 'C';

const char expr   = 'E';    //+, -
const char prim   = 'P';    //(expr), number, number^number
const char term   = 'T';    //*,/,%
const char number = '8';    // t.kind==number means that t is a number Token
// const declaration token
const std::string declkey = "let";		// declaration keyword
const std::string constkey = "const";	// const keyword
const std::string prompt  = "> ";
const std::string result  = "= "; // used to indicate that what follows is a result

#endif // SYMBOLS_H
