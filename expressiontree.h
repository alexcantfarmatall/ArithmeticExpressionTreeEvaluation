#ifndef EXPRESSIONTREE_H
#define EXPRESSIONTREE_H

#include "node.h"
#include <string>
#include "symbols.h"
#include <vector>
#include "token.h"
#include <QGraphicsScene>
#include <QGraphicsLineItem>
#include <QGraphicsEllipseItem>
#include <QGraphicsSimpleTextItem>
#include <token_stream.h>



class ExpressionTree
{

    Token_stream token_stream;

public:
    ExpressionTree(std::string);
    ~ExpressionTree();
    Node* build_tree(std::vector<Token>&);
    //void traverse(Node*);
    double evaluate(Node*);
    QGraphicsScene* tree_scene;
    void tree_graph(double,double,Node*);
    Node* root;
    double graphics_view_middle;


private:
    double calculate(char, double, double);
    int getWeight(char);

    bool isOp(char);


    const double node_circle_radius = 15;
    const double node_line_lenght = 50;
    QGraphicsTextItem* tree_tittle;
};

#endif // EXPRESSIONTREE_H
